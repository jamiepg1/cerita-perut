{{ if { url:segments segment="1" } == 'signin-home' }}
<base target="_parent" />
<style>
header, footer, h2 {display:none;}
input {width:175px;}
body {background:transparent;}
label {color:#003399;margin: 0px 10px -3px 10px;}
.btnlogin {color: #fff;background: #3d76b9;width: 194px;text-transform: uppercase;}
</style>
{{ else }}
{{ endif }}
<div> 
<h2 class="page-title" id="page_title"><?php echo lang('user:login_header') ?></h2>

<?php if (validation_errors()): ?>
<div class="error-box">
	<?php echo validation_errors();?>
</div>
<?php endif ?>

<?php echo form_open('users/login', array('id'=>'login'), array('redirect_to' => $redirect_to)) ?>
<ul style="margin:0px; padding:0px;">
	<li>
		<label for="email">Username / <?php echo lang('global:email') ?></label>
		<?php echo form_input('email', $this->input->post('email') ? $this->input->post('email') : '')?>
	</li>
	<li>
		<label for="password"><?php echo lang('global:password') ?></label>
		<input type="password" id="password" name="password" maxlength="20" />
	</li>
	<li class="form_buttons">
		<input type="submit" value="<?php echo lang('user:login_btn') ?>" name="btnLogin" class="btnlogin" /><br><br> 
		<span  style="display:none;" class="register"> Not Registered ? <?php echo anchor('register', lang('user:register_btn'));?> | <?php echo anchor('users/reset_pass', lang('user:reset_password_link'));?></span>
	</li>
</ul>
<br><br>
<?php echo form_close() ?>
</div>
