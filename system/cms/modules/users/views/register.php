<base target="_parent" />
<style>
header, footer, h2 {display:none;}
input {width:175px;}
body {background:transparent;}
label {color:#003399;margin: 0px 10px -3px 10px;}
.btnlogin input {color: #fff;background: #3d76b9;width: 194px;text-transform: uppercase;}
</style>

{{ if { url:segments segment="1" } == 'register-business' }}

<p style="display:none;">
	<span id="active_step"><?php echo lang('user:register_step1') ?></span> -&gt;
	<span><?php echo lang('user:register_step2') ?></span>
</p>

<?php if ( ! empty($error_string)):?>
<!-- Woops... -->
<div class="error-box">
	<?php echo $error_string;?>
</div>
<?php endif;?>

<?php echo form_open('register', array('id' => 'register')) ?>
<ul class="loginform">
	
	<?php if ( ! Settings::get('auto_username')): ?>
	<li>
		<label for="username"><?php echo lang('user:username') ?></label>
		<p class="instructions">Once its registered you cant change your username</p>
		<input type="text" name="username" maxlength="100" value="<?php echo $_user->username ?>" />
	</li>
	<?php endif ?>
	
	<li>
		<label for="email"><?php echo lang('global:email') ?></label>
		<p class="instructions">Enter a valid email address</p>
		<input type="text" name="email" maxlength="100" value="<?php echo $_user->email ?>" />
		<?php echo form_input('d0ntf1llth1s1n', ' ', 'class="default-form" style="display:none"') ?>
	</li>
	
	<li>
		<label for="password"><?php echo lang('global:password') ?></label><br>
		<input type="password" name="password" maxlength="100" />
	</li>

	<?php foreach($profile_fields as $field) { if($field['required'] and $field['field_slug'] != 'display_name') { ?>
	<li>
		<label for="<?php echo $field['field_slug'] ?>"><?php echo (lang($field['field_name'])) ? lang($field['field_name']) : $field['field_name'];  ?></label><br>
		<div class="input"><?php echo $field['input'] ?></div>
	</li>
	<?php } } ?>

	
	<li class="btnlogin">
		<?php echo form_submit('btnSubmit', lang('user:register_btn')) ?>
	</li>
</ul>
<?php echo form_close() ?>

{{ else }}

<p style="display:none;">
	<span id="active_step"><?php echo lang('user:register_step1') ?></span> -&gt;
	<span><?php echo lang('user:register_step2') ?></span>
</p>

<?php if ( ! empty($error_string)):?>
<!-- Woops... -->
<div class="error-box">
	<?php echo $error_string;?>
</div>
<?php endif;?>

<?php echo form_open('register', array('id' => 'register')) ?>
<ul style="margin:0px; padding:0px;">
	
	<?php if ( ! Settings::get('auto_username')): ?>
	<li>
		<label for="username"><?php echo lang('user:username') ?></label>
		<p class="instructions">Once its registered you cant change your username</p>
		<input type="text" name="username" maxlength="100" value="<?php echo $_user->username ?>" />
	</li>
	<?php endif ?>
	
	<li>
		<label for="email"><?php echo lang('global:email') ?></label>
		<p class="instructions">Enter a valid email address</p>
		<input type="text" name="email" maxlength="100" value="<?php echo $_user->email ?>" />
		<?php echo form_input('d0ntf1llth1s1n', ' ', 'class="default-form" style="display:none"') ?>
	</li>
	
	<li>
		<label for="password"><?php echo lang('global:password') ?></label><br>
		<input type="password" name="password" maxlength="100" />
	</li>

	<?php foreach($profile_fields as $field) { if($field['required'] and $field['field_slug'] != 'display_name') { ?>
	<li>
		<label for="<?php echo $field['field_slug'] ?>"><?php echo (lang($field['field_name'])) ? lang($field['field_name']) : $field['field_name'];  ?></label><br>
		<div class="input"><?php echo $field['input'] ?></div>
	</li>
	<?php } } ?>

	
	<li class="btnlogin">
		<?php echo form_submit('btnSubmit', lang('user:register_btn')) ?>
	</li>
</ul>
<?php echo form_close() ?>

{{ endif }}